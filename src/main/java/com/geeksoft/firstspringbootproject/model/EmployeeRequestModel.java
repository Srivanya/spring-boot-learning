package com.geeksoft.firstspringbootproject.model;

public class EmployeeRequestModel {

    private String EmployeeName;
    private Float EmployeeSalary;
    private int EmployeeAge;
    private String EmployeeEmail;
    private String EmployeeRole;

    public String getEmployeeName() {
        return EmployeeName;
    }

    public void setEmployeeName(String employeeName) {
        EmployeeName = employeeName;
    }

    public Float getEmployeeSalary() {
        return EmployeeSalary;
    }

    public void setEmployeeSalary(Float employeeSalary) {
        EmployeeSalary = employeeSalary;
    }

    public int getEmployeeAge() {
        return EmployeeAge;
    }

    public void setEmployeeAge(int employeeAge) {
        EmployeeAge = employeeAge;
    }

    public String getEmployeeEmail() {
        return EmployeeEmail;
    }

    public void setEmployeeEmail(String employeeEmail) {
        EmployeeEmail = employeeEmail;
    }

    public String getEmployeeRole() {
        return EmployeeRole;
    }

    public void setEmployeeRole(String employeeRole) {
        EmployeeRole = employeeRole;
    }
}
